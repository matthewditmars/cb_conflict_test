package com.example.conflict_poc;
//////////////////////////////////////////////////////////////
//// C O P Y R I G H T (c) 2019                             //
//// PUSH Design Solutions Inc. and/or its affiliates       //
//// All Rights Reserved                                    //
//////////////////////////////////////////////////////////////
////                                                        //
//// THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of         //
//// PUSH Design Solutions Inc. and/or its affiliates.      //
//// The copyright notice above does not evidence any       //
//// actual or intended publication of such source code.    //
////                                                        //
//////////////////////////////////////////////////////////////
////
//
import android.content.Context;

import com.couchbase.lite.CouchbaseLiteException;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class CouchbaseBridge {

    // Singleton references
    private static CouchbaseBridge mCouchbaseBridge = null;
    private Couchbase mCouchbase;

    // Channels
    private static final String COUCHBASE_INTERFACE_CHANNEL =
            "pushstrength.com/couchbaseInterface/";

    // Methods
private static final String INIT_COUCHBASE = "initCouchbase";
private static final String UPDATE_DOC_MULTIPLE_TIMES =
        "updateDocMultipleTimes";

    public static CouchbaseBridge initSharedCouchbaseBridge(Context applicationContext) {
        if (mCouchbaseBridge == null) {
            mCouchbaseBridge = new CouchbaseBridge(applicationContext);
        }
        return mCouchbaseBridge;
    }

    static CouchbaseBridge getSharedCouchbaseBridge() {
        return mCouchbaseBridge;
    }

    private CouchbaseBridge(Context applicationContext) {
        mCouchbase = Couchbase.initSharedCouchbaseInstance(applicationContext);
    }

    public void setupMethodChannels(BinaryMessenger binaryMessenger) {
        try {

            MethodChannel channel = new MethodChannel(binaryMessenger, COUCHBASE_INTERFACE_CHANNEL);
            setup(channel);

        } catch (Exception e) {
        }
    }

    private void setup(MethodChannel channel) {
        channel.setMethodCallHandler((MethodCall methodCall, MethodChannel.Result result) -> {
            switch (methodCall.method) {
                case INIT_COUCHBASE:
                    handleInitCouchbase(result);
                    break;
                case UPDATE_DOC_MULTIPLE_TIMES:
                    handleUpdateDocMultipleTimes(result, methodCall.arguments);
                    break;
                default:
                    result.notImplemented();
            }
        });
    }

    private void handleInitCouchbase(MethodChannel.Result result) {
        try {
            mCouchbase.initCouchbase();
            result.success(null);
        } catch (CouchbaseLiteException e) {
            result.error("FAILED TO SETUP DATABASE", "Error setting up database", e.toString());
        }
    }

    private void handleUpdateDocMultipleTimes(MethodChannel.Result result,
                                              Object arguments) {
        try {
            int numTimes = (int) arguments;
            mCouchbase.updateDocMultipleTimes(numTimes);
        } catch (CouchbaseLiteException e) {
            result.error("failed to update doc", "error upadting doc", e.toString());
        }
    }

}
