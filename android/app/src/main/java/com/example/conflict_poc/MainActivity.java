package com.example.conflict_poc;


import io.flutter.embedding.android.FlutterActivity;
import androidx.annotation.NonNull;

import io.flutter.embedding.engine.FlutterEngine;

public class MainActivity extends FlutterActivity {
    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        CouchbaseBridge
                .initSharedCouchbaseBridge(getApplicationContext())
                .setupMethodChannels(flutterEngine.getDartExecutor().getBinaryMessenger());
    }
}
