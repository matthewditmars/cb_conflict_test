package com.example.conflict_poc;
//
////
//////////////////////////////////////////////////////////////
//// C O P Y R I G H T (c) 2019                             //
//// PUSH Design Solutions Inc. and/or its affiliates       //
//// All Rights Reserved                                    //
//////////////////////////////////////////////////////////////
////                                                        //
//// THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of         //
//// PUSH Design Solutions Inc. and/or its affiliates.      //
//// The copyright notice above does not evidence any       //
//// actual or intended publication of such source code.    //
////                                                        //
//////////////////////////////////////////////////////////////
////
//
import com.couchbase.lite.*;
import android.content.Context;
import java.net.URI;
import java.net.URISyntaxException;


public class Couchbase {

    // ~~~~~~~~~~~~~~~~~~ COUCHBASE CONSTANTS ~~~~~~~~~~~~~~~~~~
    private static final String DATABASE_NAME = "pushdata";

    // Member Variables
    private Database mDatabase;
    private Context context;
    private Replicator replicator;

    private static Couchbase couchbase = null;

    public static Couchbase initSharedCouchbaseInstance(Context applicationContext) {
        if (couchbase == null) {
            couchbase = new Couchbase(applicationContext);
        }
        return couchbase;
    }

    public static Couchbase getSharedCouchbaseInstance() {
        return couchbase;
    }

    private Couchbase(Context applicationContext){
        this.context = applicationContext;
    }

    public void initCouchbase() throws CouchbaseLiteException {
        initDatabase();
        initAndStartReplicator();
    }

    private void initDatabase() throws CouchbaseLiteException {
        CouchbaseLite.init(context);
        final DatabaseConfiguration config = new DatabaseConfiguration();

        mDatabase = new Database(DATABASE_NAME, config);
    }

    private void initAndStartReplicator() {
        // Create replicators to push and pull changes to and from the cloud.
        Endpoint targetEndpoint = null;
        try {
            targetEndpoint = new URLEndpoint(new URI("ws://20.84.60" +
                    ".73:4984/db"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        ReplicatorConfiguration replConfig =
                new ReplicatorConfiguration(mDatabase, targetEndpoint);
        replConfig.setReplicatorType(ReplicatorConfiguration.ReplicatorType.PUSH_AND_PULL);
        replConfig.setContinuous(true);

        // Create replicator (be sure to hold a reference somewhere that will prevent the Replicator from being GCed)
        replicator = new Replicator(replConfig);

        replicator.start();
    }

    public MutableDocument getTestDocument() {
        MutableDocument document = mDatabase.getDocument("androidTestDoc").toMutable();
        return document;
    }

    public void updateDocMultipleTimes(int timesToUpdate) throws CouchbaseLiteException {
        MutableDocument docToUpdate = getTestDocument();

        if(docToUpdate != null) {
            for (int i = 0; i < timesToUpdate; i++) {
                docToUpdate.setString("updatedString",
                        "doc updated at: " + System.currentTimeMillis());
                mDatabase.save(docToUpdate);
            }
        }

    }
}



