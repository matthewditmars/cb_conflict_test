/**
* //////////////////////////////////////////////////////////
* // C O P Y R I G H T (c) 2021                           //
* // PUSH Design Solutions Inc. and/or its affiliates     //
* // All Rights Reserved                                  //
* //////////////////////////////////////////////////////////
* //                                                      //
* // THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of       //
* // PUSH Design Solutions Inc. and/or its affiliates.    //
* // The copyright notice above does not evidence any     //
* // actual or intended publication of such source code.  //
* //                                                      //
* //////////////////////////////////////////////////////////
*/

import 'dart:async';
import 'package:flutter/services.dart';

class CouchbaseInterfaceBloc {
  static final CouchbaseInterfaceBloc _singleton =
      CouchbaseInterfaceBloc._internal();

  factory CouchbaseInterfaceBloc() {
    return _singleton;
  }

  CouchbaseInterfaceBloc._internal();

  static const _platform =
      MethodChannel("pushstrength.com/couchbaseInterface/");

  /*
   * Starts the database on the Native Side
   */
  Future<void> initializeCouchbase() async {
    await _platform.invokeMethod('initCouchbase');
  }

  Future<void> updateDocMultipleTimes(int numTimes) async {
    await _platform.invokeMethod('updateDocMultipleTimes', numTimes);
  }
}
