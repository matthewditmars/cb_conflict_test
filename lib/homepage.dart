/**
* //////////////////////////////////////////////////////////
* // C O P Y R I G H T (c) 2021                           //
* // PUSH Design Solutions Inc. and/or its affiliates     //
* // All Rights Reserved                                  //
* //////////////////////////////////////////////////////////
* //                                                      //
* // THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of       //
* // PUSH Design Solutions Inc. and/or its affiliates.    //
* // The copyright notice above does not evidence any     //
* // actual or intended publication of such source code.  //
* //                                                      //
* //////////////////////////////////////////////////////////
*/

import 'package:conflict_poc/couchbaseinterface.dart';
/**
* //////////////////////////////////////////////////////////
* // C O P Y R I G H T (c) 2021                           //
* // PUSH Design Solutions Inc. and/or its affiliates     //
* // All Rights Reserved                                  //
* //////////////////////////////////////////////////////////
* //                                                      //
* // THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of       //
* // PUSH Design Solutions Inc. and/or its affiliates.    //
* // The copyright notice above does not evidence any     //
* // actual or intended publication of such source code.  //
* //                                                      //
* //////////////////////////////////////////////////////////
*/

import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isCouchbaseInit = false;

  Future<void> initCouchbase() async {
    try {
      await CouchbaseInterfaceBloc().initializeCouchbase();
      setState(() {
        isCouchbaseInit = true;
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: isCouchbaseInit
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextButton(
                    onPressed: () async => await CouchbaseInterfaceBloc()
                        .updateDocMultipleTimes(1),
                    child: Text("write 1 version"),
                  ),
                  TextButton(
                    onPressed: () async => await CouchbaseInterfaceBloc()
                        .updateDocMultipleTimes(5),
                    child: Text("write 5 version"),
                  ),
                  TextButton(
                    onPressed: () async => await CouchbaseInterfaceBloc()
                        .updateDocMultipleTimes(10),
                    child: Text("write 10 version"),
                  ),
                  TextButton(
                    onPressed: () async => await CouchbaseInterfaceBloc()
                        .updateDocMultipleTimes(100),
                    child: Text("write 100 version"),
                  ),
                  TextButton(
                    onPressed: () async => await CouchbaseInterfaceBloc()
                        .updateDocMultipleTimes(1000),
                    child: Text("write 1000 version"),
                  ),
                ],
              ),
            )
          : Center(
              child: TextButton(
                onPressed: () async => await initCouchbase(),
                child: Text("init couchbase"),
              ),
            ),
    );
  }
}
