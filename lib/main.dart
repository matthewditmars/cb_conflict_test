/**
* //////////////////////////////////////////////////////////
* // C O P Y R I G H T (c) 2021                           //
* // PUSH Design Solutions Inc. and/or its affiliates     //
* // All Rights Reserved                                  //
* //////////////////////////////////////////////////////////
* //                                                      //
* // THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE of       //
* // PUSH Design Solutions Inc. and/or its affiliates.    //
* // The copyright notice above does not evidence any     //
* // actual or intended publication of such source code.  //
* //                                                      //
* //////////////////////////////////////////////////////////
*/

import 'package:conflict_poc/homepage.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
